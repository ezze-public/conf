FROM ubuntu:20.04
ARG DEBIAN_FRONTEND=noninteractive

# apt
RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get -y install sudo screen nano iputils-ping wget nload htop net-tools git cron libcurl4 ssh

# git
RUN wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/git/installer | bash

# ssh
RUN sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config
RUN service ssh restart

# php
RUN apt-get -y install software-properties-common apt-transport-https
RUN add-apt-repository ppa:ondrej/php -y
RUN apt-get -y update
RUN apt-get -y upgrade
#
RUN apt-get -y install nginx
RUN apt-get -y install php7.4-fpm php7.4-curl php7.4-zip php7.4-gd php7.4-mysql php7.4-xml php7.4-mbstring php7.4-intl
RUN service php7.4-fpm start
#
RUN ln -s /var/run/php/php7.4-fpm.sock /var/run/php.sock
RUN ln -s /etc/php/7.4 /etc/php/x
RUN ln -s /usr/sbin/php-fpm7.4 /usr/sbin/php-fpm
RUN ln -s /etc/init.d/php7.4-fpm /etc/init.d/php-fpm
#
RUN rm -rf /etc/nginx/sites-enabled
RUN ln -s /etc/nginx/sites-available /etc/nginx/sites-enabled
#
RUN echo "listen = /var/run/php.sock" >> /etc/php/x/fpm/php-fpm.conf
RUN echo "listen.owner = www-data" >>    /etc/php/x/fpm/php-fpm.conf
RUN echo "listen.group = www-data" >>    /etc/php/x/fpm/php-fpm.conf
RUN echo "listen.mode = 0660" >>         /etc/php/x/fpm/php-fpm.conf
#
RUN wget https://gitlab.com/ezze-public/conf/-/raw/main/nginx-php/conf/default -O    /etc/nginx/sites-available/default
RUN wget https://gitlab.com/ezze-public/conf/-/raw/main/nginx-php/conf/nginx.conf -O /etc/nginx/nginx.conf
RUN wget https://gitlab.com/ezze-public/conf/-/raw/main/nginx-php/conf/www.conf -O   /etc/php/x/fpm/pool.d/www.conf
RUN wget https://gitlab.com/ezze-public/conf/-/raw/main/nginx-php/conf/php7.4.ini -O    /etc/php/x/fpm/php.ini
#
RUN cd /etc/php/x; rm -rf cli/php.ini ; ln -s /etc/php/x/fpm/php.ini ./cli/php.ini
#
RUN if [ ! -d /run/php ]; then mkdir /run/php; fi
RUN /etc/init.d/php-fpm restart
RUN service nginx reload

# sudo
RUN echo "www-data ALL=NOPASSWD: ALL" >> /etc/sudoers

# cron
RUN update-rc.d cron defaults
RUN echo "/usr/sbin/php-fpm" > /etc/rc.local
RUN echo "service ssh start" >> /etc/rc.local
RUN chmod 0777 /etc/rc.local

WORKDIR /var/www

# injected
ADD dockerpatc[h] /tmp/dockerpatch
RUN if [ -f "/tmp/dockerpatch" ]; then bash /tmp/dockerpatch; rm -rf /tmp/dockerpatch; fi
#-PATCH-HERE-#

# end
STOPSIGNAL SIGTERM
CMD /etc/rc.local && /usr/sbin/cron start && /usr/sbin/nginx -g 'daemon off;' && /usr/sbin/php-fpm
