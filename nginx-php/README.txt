
# ♔
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/nginx-php/README.txt | bash -s phpv name port <patch> <etc>

phpv=$1
name=$2
port=$3
patch=$4
etc=$5

wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/docker/README.txt | bash

wget -O /tmp/dockerfile https://gitlab.com/ezze-public/conf/-/raw/main/nginx-php/conf/dockerfile
sed -i 's/7.4/'$1'/g' /tmp/dockerfile

if [ "$patch" != "" ] && [ "$patch" != "-" ]; then
  wget -qO- $patch >> dockerpatch
fi

# docker system prune -af
image=$2"-image"
docker build -t $image -f /tmp/dockerfile .
# rm -rf /tmp/dockerfile

name=$2
port=$3
sshp=$(($3-1))

docker run -t -d --restart unless-stopped --name $name --hostname $name -p $port:80 -p $sshp:22 $etc $image

