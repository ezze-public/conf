#
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/npm-on-docker/README.txt | bash -s npm 3022 3000

docker rm -f $1
docker rmi npm-image

docker build -t npm-image https://gitlab.com/ezze-public/conf/-/raw/main/npm-on-docker/conf/dockerfile

if [ "$3" != "" ]; then
  port3k="-p $3:3000"
fi

cmd="docker run -t -d --restart unless-stopped --hostname $1.ezze.li --name $1 -p $2:22 "$port3k" -v /root/.ssh:/root/.ssh -v /docker/$1/home:/home npm-image"

echo $cmd
$cmd

