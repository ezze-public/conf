#
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/nginx-on-docker/README.txt | bash -s the-host 30080 30443 30022

if [ ! -f /docker/$1/sites-available/0-default ]; then
    mkdir -p /docker/$1/sites-available/
    wget -O /docker/$1/sites-available/0-default https://gitlab.com/ezze-public/conf/-/raw/main/nginx-on-docker/conf/0-default
fi

docker build -t nginx https://gitlab.com/ezze-public/conf/-/raw/main/nginx-on-docker/conf/dockerfile

if [ "$3" != "" ]; then
  port443="-p $3:443"
fi

if [ "$4" != "" ]; then
  port22="-p $4:22"
fi

cmd="docker run -t -d --restart unless-stopped --link php7 --link php8 --hostname $1.ezze.li -v /docker/$1/home:/home -v /docker/$1/sites-available:/etc/nginx/sites-available --name $1 -p $2:80 "$port443" "$port22" --net rick nginx"

echo $cmd
$cmd
