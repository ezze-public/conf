
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/nodejs-on-docker/README.txt | bash

cd && curl -sL https://deb.nodesource.com/setup_18.x -o /tmp/nodesource_setup.sh
sudo bash /tmp/nodesource_setup.sh
rm -rf /tmp/nodesource_setup.sh
sudo apt-get install -y nodejs
npm i -g express
npm i -g nodemon
