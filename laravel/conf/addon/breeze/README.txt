
#
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/laravel/conf/addon/breeze/README.txt | bash

sudo -u www-data composer require laravel/breeze --dev
sudo -u www-data php artisan breeze:install blade --no-interaction
sudo -u www-data npm install
# sudo -u www-data npm run dev
sudo -u www-data php artisan migrate
