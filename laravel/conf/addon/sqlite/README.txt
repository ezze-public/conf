
#
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/laravel/conf/addon/sqlite/README.txt | bash
#

apt -y install php-sqlite3 sqlite3
apt install php-sqlite3
service php8.3-fpm restart

sudo -u www-data php artisan make:command ConfigureSQLite

sudo -u www-data wget 'https://gitlab.com/ezze-public/conf/-/raw/main/laravel/conf/addon/sqlite/conf/ConfigureSQLite.php' -O /var/www/app/Console/Commands/ConfigureSQLite.php

sudo -u www-data php artisan config:ConfigureSQLite

sudo -u www-data php artisan config:clear
sudo -u www-data php artisan config:cache
