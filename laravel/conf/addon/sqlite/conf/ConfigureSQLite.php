<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class ConfigureSQLite extends Command
{
    protected $signature = 'config:ConfigureSQLite';
    protected $description = 'Configure the application to use SQLite as the database';

    public function handle()
    {
        $dbPath = database_path('database.sqlite');

        // Create SQLite file if it doesn't exist
        if (!File::exists($dbPath)) {
            File::put($dbPath, '');
            $this->info('SQLite database file created.');
        }

        // Update .env file
        $envPath = base_path('.env');
        if (File::exists($envPath)) {

            $envContent = File::get($envPath);

            $envContent = preg_replace('/^DB_CONNECTION=.*/m', 'DB_CONNECTION=sqlite', $envContent);
            $envContent = preg_replace('/^#?\s*DB_DATABASE=.*/m', 'DB_DATABASE=' . $dbPath, $envContent);

            File::put($envPath, $envContent);
            $this->info('.env file updated for SQLite.');
        } else {
            $this->error('.env file not found.');
        }

        $this->info('SQLite configuration complete.');
    }
}

