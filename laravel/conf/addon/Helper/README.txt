
#
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/laravel/conf/addon/Helper/README.txt | bash

cd /var/www

sudo -u www-data wget -O app/Console/Commands/ConfigureHelper.php https://gitlab.com/ezze-public/conf/-/raw/main/laravel/conf/addon/Helper/conf/ConfigureHelper.php
sudo -u www-data php artisan helper:configure
