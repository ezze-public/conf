
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/laravel/conf/addon/PubSub/README.txt | bash


wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/pm2/README.txt | bash

composer update
composer require predis/predis

sudo -u www-data wget -O app/Console/Commands/pubsubListener.php https://gitlab.com/ezze-public/conf/-/raw/main/laravel/conf/addon/PubSub/conf/pubsubListener.php
sudo -u www-data wget -O app/Http/Controllers/pubsubController.php https://gitlab.com/ezze-public/conf/-/raw/main/laravel/conf/addon/PubSub/conf/pubsubController.php

sudo -u www-data wget -O pubsub.pm2.yml https://gitlab.com/ezze-public/conf/-/raw/main/laravel/conf/addon/PubSub/conf/pubsub.pm2.yml
pm2 start pubsub.pm2.yml
pm2 save
