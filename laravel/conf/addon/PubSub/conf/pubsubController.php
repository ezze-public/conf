<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class pubsubController extends Controller
{

    // pubsubController::publish('Telegram', 'New message from Binance.');
    // 
    public static function publish( $channel, $message ){

        Redis::publish($channel, $message);

        return response()->json([
            'status' => 'OK',
            'channel' => $channel,
            'message' => $message,
        ], 200);
        
    }
}
