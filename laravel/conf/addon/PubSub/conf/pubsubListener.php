<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class pubsubListener extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pubsub:listener';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Binded to find new redis pubsub requests.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        
        echo 'Start listening for redis pubsub request ..' . PHP_EOL;

        # https://laravel.com/docs/11.x/redis
        Redis::psubscribe(['*'], function (string $message, string $channel) {
            echo $channel ." -> ". $message . PHP_EOL;
            logg($channel ." -> ".$message);
            // ...
        });

    }
}