
#
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/laravel/README.txt | bash -s <ver:8/11>
# 

ver=$1

apt -y install unzip 7zip

rm -rf /tmp/laravel
composer create-project laravel/laravel:^$ver /tmp/laravel

if [ -d /var/www-old-data ]; then rm -rf /var/www-old-data; fi
mkdir /var/www-old-data && mv /var/www/* /var/www/.[!.]* /var/www-old-data
mv /tmp/laravel/.[!.]* /tmp/laravel/* /var/www/
rm -rf /tmp/laravel

cd /var/www-old-data && mv conf README.txt /var/www/
cd /var/www-old-data && mv .git /var/www/
rm -rf /var/www/README.md

chown -R www-data:www-data /var/www
chmod -R 0777 /var/www/storage

cd /var/www
sudo -u www-data composer update
sudo -u www-data cp .env.example .env

sudo -u www-data php artisan key:generate
sudo -u www-data php artisan config:clear
sudo -u www-data php artisan config:cache

wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/laravel/conf/addon/sqlite/README.txt | bash

sudo -u www-data php artisan migrate --force
echo '/database/database.sqlite' >> .gitignore

echo '* * * * * cd /var/www && sudo -u www-data php artisan schedule:run >> /dev/null 2>&1' >> /var/www/conf/cron
(crontab -l 2>/dev/null; cat /var/www/conf/cron) | crontab -



