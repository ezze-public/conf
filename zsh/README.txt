
# 
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/zsh/README.txt | bash


apt install -y zsh
RUNZSH=no sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
chsh -s $(which zsh) $USER
zsh
echo 'export DISABLE_UPDATE_PROMPT=true' >> ~/.zshrc && source ~/.zshrc
