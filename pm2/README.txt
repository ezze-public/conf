# 
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/pm2/README.txt | bash

if [ ! -f /usr/bin/npm ]; then
  wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/npm/README.txt | bash -s 20
fi

npm i -g pm2
echo '/usr/bin/pm2 resurrect > /root/log.pm2' >> /etc/rc.local

