# 
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/php7-on-docker/README.txt | bash -s nginx

docker build -t php7 https://gitlab.com/ezze-public/conf/-/raw/main/php7-on-docker/conf/dockerfile
docker run -t -d --restart unless-stopped -v /docker/$1/home:/home -p 22907:22 --net rick --hostname php7 --name php7 php7

