# port: 3028
# wget -qO https://gitlab.com/ezze-public/conf/-/raw/main/sftp-on-docker/README.txt | bash

wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/docker/README.txt | bash

docker build -t sftp-image https://gitlab.com/ezze-public/conf/-/raw/main/sftp-on-docker/conf/dockerfile
docker run -t -d --restart unless-stopped --name sftp --hostname sftp.ezze.li -v /docker/sftp:/sftp -p 3028:22 sftp-image

