
#
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/ssh/README.txt | bash

apt -y update
apt -y install ssh

sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config
service ssh restart
