#
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/vue-on-docker/README.txt | bash -s vue 3022 3000

name=$1
sshP=$2
webP=$3

docker rm -f $name

wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/npm-on-docker/README.txt | bash -s $name $sshP $webP

docker exec -t $name sh -c " cd /home && wget -O vue.tar.gz 'https://gitlab.com/ezze-public/conf/-/raw/main/vue-on-docker/lib/vue.tar.gz?inline=false' && tar -xzf vue.tar.gz && rm -rf vue.tar.gz && cd vue && npm i"
docker exec -t $name sh -c "echo 'cd /home/vue && npm run dev -- --host &' >> /etc/rc.local"

docker restart $name
