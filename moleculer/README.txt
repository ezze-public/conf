
#
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/moleculer/README.txt | bash -s code

wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/npm/README.txt | bash -s 20
npm i -g moleculer-cli

echo ""
echo "####"
echo "moleculer init project "$1
echo "####"
echo ""

cd && moleculer init project $1
