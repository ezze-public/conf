#
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/mysql-on-docker/README.txt | bash -s fsql 43

# docker network create --subnet=172.20.0.0/16 rick
# docker run -d --name mariadb -p 3306:3306 -eMARIADB_ROOT_PASSWORD=$pass --net rick --ip 172.20.0.33 mariadb/server:10.3
# docker run -d --name pma --net rick -e PMA_HOST=172.20.0.33 -p 6060:80 phpmyadmin

if [ $(docker network ls | grep rick | awk {'print $2'}) != "rick" ]; then
    docker network create --subnet=172.18.0.0/16 rick
fi

ymlpath="/root/sql_"$1
if [ ! -d $ymlpath ]; then
    mkdir -p $ymlpath
fi
cd $ymlpath

ip=$(hostname -I | awk {'print $1'})
sql_pass=$(md5sum /etc/passwd | awk {'print $1'})
port_pfx=$2
sql_port=$2'06'
pma_port=$((port_pfx+1))'06'

wget -O docker-compose.yml https://gitlab.com/ezze-public/conf/-/raw/main/mysql-on-docker/conf/docker-compose.yml
echo 'NETWORK=rick
POSTFIX='$1'
MYSQL_ROOT_PASSWORD='$sql_pass'
MYSQL_LOCAL_IP=172.18.0.'$2'
MYSQL_PUBLIC_PORT='$sql_port'
PMA_LOCAL_IP=172.18.0.'$((port_pfx+1))'
PMA_PUBLIC_PORT='$pma_port'
' > .env

# docker-compose up -d
# sleep 1
# dkl

# echo '[client]' > ~/.my.cnf
# echo 'user=root' >> ~/.my.cnf
# echo 'password='$sql_pass >> ~/.my.cnf

echo "run: docker-compose up -d"
echo ""
echo "pma: http://"$ip":"$pma_port"/"
echo "user: root"
echo "pass: "$sql_pass

echo ""

