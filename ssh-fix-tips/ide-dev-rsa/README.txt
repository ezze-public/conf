
# 
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/ssh-fix-tips/ide-dev-rsa/README.txt | bash

if [ ! -d /root/.ssh ]; then
  mkdir /root/.ssh
fi

wget -O /root/.ssh/authorized_keys https://gitlab.com/ezze-public/conf/-/raw/main/ssh-fix-tips/ide-dev-rsa/authorized_keys
chmod 0600 /root/.ssh/authorized_keys

