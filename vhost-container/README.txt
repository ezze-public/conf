# 
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/vhost-container/README.txt | bash
# just take care of crontab

wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/php7-on-docker/README.txt  | bash -s vhost
wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/php8-on-docker/README.txt  | bash -s vhost
wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/nginx-on-docker/README.txt | bash -s vhost 80 443 8022

docker exec vhost mkdir /root/.ssh
docker exec vhost wget -O /root/.ssh/authorized_keys https://gitlab.com/ezze-public/conf/-/raw/main/vhost-container/conf/.ssh/authorized_keys
docker exec vhost wget -O /root/.ssh/id_rsa.pub https://gitlab.com/ezze-public/conf/-/raw/main/vhost-container/conf/.ssh/id_rsa.pub
docker exec vhost wget -O /root/.ssh/id_rsa https://gitlab.com/ezze-public/conf/-/raw/main/vhost-container/conf/.ssh/id_rsa
docker exec vhost chmod 0600 /root/.ssh/authorized_keys /root/.ssh/id_rsa


