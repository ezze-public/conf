
#
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/nginx-php/README.txt | bash -s name port <phpv> <etc> <patch>

name=$1
port=$2
phpv=$3
etc=$4
patch=$5

echo "name: "$name
echo "port: "$port
echo "phpv: "$phpv
echo "etc: "$etc
echo "patch: "$patch

if [ "$phpv" == "-" ] || [ "$phpv" == "" ]; then
  phpv="7.4"
fi

if [ "$etc" == "-" ]; then
  etc=""
fi

sshp=$(($port-1))
etc=$etc' -p '$port':80 -p '$sshp':22 -v /root/.ssh:/root/.ssh'

if [ "$patch" != "" ] && [ "$patch" != "-" ]; then
  echo -e "\n$(wget -qO- $patch)\n$( if [ -f dockerpatch ]; then cat dockerpatch; fi )" > dockerpatch
fi

patch=$(wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/nginx-php/conf/patch | sed 's/7.4/'$phpv'/g')
echo -e "\n$patch\n$( if [ -f dockerpatch ]; then cat dockerpatch; fi )" > dockerpatch

wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/raw-ubuntu/README.txt | bash -s $name "$etc"
