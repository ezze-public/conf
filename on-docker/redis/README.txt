
#
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/redis/README.txt | bash

pass=$(md5sum /etc/passwd | awk {'print $1'})

docker build -t redis-image https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/redis/conf/dockerfile --build-arg="REDIS_PASSWORD="$pass
docker run -t -d --restart unless-stopped --name redis -p 6379:6379 --hostname redis redis-image

docker exec redis bash -c "echo "$pass" > /root/redis-password"
echo -e "\n####\nredis password: "$pass"\n####\n"
