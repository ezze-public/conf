
# 
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/laravel/private/README.txt | bash -s name <repo> <etc> <patch>

name=$1
repo=$2
etc=$3
patch=$4

if [ "$patch" != "" ] && [ "$patch" != "-" ]; then
  patch=$(wget -qO- $patch)
  echo -e "\n$patch\n$( if [ -f dockerpatch ]; then cat dockerpatch; fi )" > dockerpatch
fi

if [ "$repo" == "-" ] ; then
  repo=""
fi

if [ "$etc" == "-" ]; then
  etc=""
fi
etc=$etc' -v /root/.ssh:/root/.ssh'

patch=$(wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/laravel/private/conf/patch)
echo -e "\n$patch\n$( if [ -f dockerpatch ]; then cat dockerpatch; fi )" > dockerpatch

wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/php/README.txt | bash -s $name 8.2 "$etc"

if [ "$repo" == "" ]; then
  cmd=' wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/laravel/private/conf/init-without-repo | bash '

else
  cmd=' wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/laravel/private/conf/init | sed 's/#REPO#/'$repo'/g' | bash  '
fi

# echo $cmd
docker exec $name bash -c "$cmd"
