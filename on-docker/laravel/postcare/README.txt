
# 
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/laravel/postcare/README.txt | bash

if [ ! -f /var/www/.env ]; then
    echo "conf file .env not defined"
    exit;
fi

# get into the pitch
cd /var/www

# prepare laravel permissions
chown -R www-data:www-data /var/www
chmod -R 0777 storage
composer update

# init
if [ -f "conf/init.sh" ]; then
    bash conf/init.sh
fi

# cache
php artisan key:generate
php artisan config:clear
php artisan config:cache

php artisan migrate

# set the queue
wget https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/laravel/postcare/conf/queue.pm2.yml -O queue.pm2.yml
pm2 start queue.pm2.yml

# rc:start
if [ ! -f "/var/www/app/Console/Commands/rcStart.php" ]; then
    php artisan make:command rcStart --command=rc:start
    mkdir -p app/Console/Commands/
    wget -O app/Console/Commands/rcStart.php https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/laravel/postcare/conf/rcStart.php
fi
#
wget https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/laravel/postcare/conf/rc.pm2.yml -O rc.pm2.yml
pm2 start rc.pm2.yml

# save pm2
pm2 save
pm2 startup

# set the cron
crontab <<EOF
* * * * * cd /var/www && php artisan schedule:run >> /dev/null 2>&1
EOF

pm2 status
