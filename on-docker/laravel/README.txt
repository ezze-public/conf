
# 
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/laravel/README.txt | bash -s name <port> <repo> <etc> <patch>

name=$1
port=$2
repo=$3
etc=$4
patch=$5

if [ "$port" == "" ] || [ "$port" == "-" ]; then
    echo "trying laravel private .."
    wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/laravel/private/README.txt | bash -s $name $repo "$etc" $patch

else
    echo "trying laravel public .."
    wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/laravel/public/README.txt | bash -s $name $port $repo "$etc" $patch
fi

