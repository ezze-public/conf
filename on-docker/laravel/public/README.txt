
# 
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/laravel/public/README.txt | bash -s name port <etc> <patch>

name=$1
port=$2
etc=$3
patch=$4

if [ "$patch" != "" ] && [ "$patch" != "-" ]; then
  patch=$(wget -qO- $patch)
  echo -e "\n$patch\n$( if [ -f dockerpatch ]; then cat dockerpatch; fi )" > dockerpatch
fi

patch=$(wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/laravel/public/conf/patch)
echo -e "\n$patch\n$( if [ -f dockerpatch ]; then cat dockerpatch; fi )" > dockerpatch

if [ "$etc" == "-" ]; then
  etc=""
fi
etc=$etc' -v /root/.ssh:/root/.ssh'

# phpv=8.2
phpv=8.3

wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/nginx-php/README.txt | bash -s $name $port $phpv "$etc"
docker exec $name bash -c "wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/laravel/public/conf/init | bash"

