<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class rcStart extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rc:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        while (true) {
            sleep(1);
            echo ".";
        }
        // return Command::SUCCESS;
    }
}
