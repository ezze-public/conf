
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/laravel/conf/Mongo/README.txt | bash

apt -y update
apt -y install php-mongodb

composer update
composer require mongodb/laravel-mongodb

wget -O config/database.php https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/laravel/conf/Mongo/conf/database.php