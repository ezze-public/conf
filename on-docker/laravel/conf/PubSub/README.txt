
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/laravel/pubsub/README.txt | bash

composer update
composer require predis/predis

wget -O app/Console/Commands/publishListener.php https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/laravel/conf/PubSub/conf/publishListener.php
wget -O publish.pm2.yml https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/laravel/conf/PubSub/conf/publish.pm2.yml
pm2 start publish.pm2.yml

