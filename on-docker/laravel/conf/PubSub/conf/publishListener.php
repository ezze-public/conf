<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class publishListener extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'publish:listener';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Binded to find new redis publish requests.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        
        echo 'Start listening for redis publish request ..' . PHP_EOL;

        # https://laravel.com/docs/11.x/redis
        Redis::psubscribe(['*'], function (string $message, string $channel) {
            echo $channel ." -> ". $message . PHP_EOL;
            // ...
        });

    }
}