
#
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/php/README.txt | bash -s name <phpv> <etc> <patch>

name=$1
phpv=$2
etc=$3
patch=$4

if [ "$phpv" == "-" ] || [ "$phpv" == "" ]; then
  phpv="7.4"
fi

if [ "$etc" == "-" ]; then
  etc=""
fi

sshp="22"$( echo $phpv | sed -r 's/\.//g' )
etc=$etc' -p '$sshp':22'

if [ "$patch" != "" ] && [ "$patch" != "-" ]; then
  patch=$(wget -qO- $patch)
  echo "\n$patch\n$( if [ -f dockerpatch ]; then cat dockerpatch; fi )" > dockerpatch
fi

patch=$(wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/php/conf/patch | sed 's/7.4/'$phpv'/g')
echo "\n$patch\n$( if [ -f dockerpatch ]; then cat dockerpatch; fi )" > dockerpatch

wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/raw-ubuntu/README.txt | bash -s $name "$etc"

rm -rf dockerpatch

