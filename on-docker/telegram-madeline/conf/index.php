<?php

ini_set('display_errors', 'On');
ini_set('error_reporting', 'E_ALL');
ini_set('memory_limit', '-1');

// PHP 8.2+ is required.

/* 
    binance_announcements
    Kucoin_news
    Bybit_Announcements
    bfxannouncements
    MEXCofficialNews
*/

if( !file_exists('madeline.php') )
    die('not installed');


include 'madeline.php';
$MadelineProto = new \danog\MadelineProto\API('session.madeline');

if(! array_key_exists('do', $_REQUEST) ){
    http_response_code(200);
    die(json_encode([
        
        'do' => [
            'sendMessage' => [
                'username' => 'username of receiver',
                'message' => 'the text you wanna send'
            ], 
            'getMessage' => [
                'channel' => 'the channel name',
                'count' => 'count of last messages',
                'onlyMessage' => 'specify if you need the whole response, or only the text of message'
            ]
        ]

    ]));

} else switch( $_REQUEST['do'] ){

    case 'sendMessage': 
        if(! $username = $_REQUEST['username'] ){
            die('no destination defined.');
        } else if(! $message = $_REQUEST['message']){
            die('no message defined');
        } else {
            http_response_code(200);
            $MadelineProto->messages->sendMessage([ 'peer' => '@'.$username, 'message' => $message ]);
        }
        die(json_encode([ 'status' => 'OK']));

    case 'getMessage':
        if(! $channelName = $_REQUEST['channel'] )
            die('no channel defined');
        $count = ( array_key_exists('count', $_REQUEST) and is_numeric($_REQUEST['count']) ) ? $_REQUEST['count'] : 10;
        $onlyMessage = ( array_key_exists('onlyMessage', $_REQUEST) and $_REQUEST['onlyMessage'] ) ? true : false;
        $messages_Messages = $MadelineProto->messages->getHistory([
            'peer' => '@'.$channelName,
            // 'offset_id' => 0,
            'offset_date' => date('U') + 3600 /* *24 */,
            // 'add_offset' => 0,
            'limit' => $count,
            // 'max_id' => 2147483646,
            // 'min_id' => 0,
            'hash' => 0
        ]);
        http_response_code(200);
        if( $onlyMessage ){
            $msg_s = [];
            foreach( $messages_Messages['messages'] as $msg )
                if( array_key_exists('message', $msg) and is_string($msg['message']) )
                    $msg_s[] = trim($msg['message'],"\r\n\t ");
            echo json_encode([ 'status'=>'OK', 'res'=>$msg_s ]);
        } else {
            echo json_encode([ 'status'=>'OK', 'res'=>$messages_Messages ]);
        }
        die;
    
}
