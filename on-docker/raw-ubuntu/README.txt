
#
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/raw-ubuntu/README.txt | bash -s name <etc> <patch>

name=$1
etc=$2
patch=$3

echo ""
echo "#####################"
echo "name: "$1
echo "etc: "$2
echo "patch: "$3
echo "#####################"
echo ""

image=$name"-image"

if [ "$etc" == "-" ]; then
    etc=""
fi

wget -O /tmp/dockerfile https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/raw-ubuntu/conf/dockerfile

if [ "$patch" != "" ] && [ "$patch" != "-" ]; then
    echo -e "\n$(wget -qO- $patch)\n$( if [ -f dockerpatch ]; then cat dockerpatch; fi )" > dockerpatch
fi

wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/docker/README.txt | bash

docker build -t $image -f /tmp/dockerfile .
rm -rf dockerpatch

echo "docker run -t -d --restart unless-stopped --name "$name" "$etc" "$image
docker run -t -d --restart unless-stopped --name $name $etc $image

