
# 
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/pma/README.txt | bash -s <host> <port>

host=$1
port=$2

docker run --name pma -d -e PMA_HOST=$host -e PMA_PORT=$port -p 6060:80 phpmyadmin/phpmyadmin
