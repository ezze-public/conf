
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/mongodb/README.txt | bash -s name <pass> <port>

if [ "$1" == "" ]; then
    echo "no name defined"
    exit
else
    name=$1
fi

if [ "$2" == "" ] || [ "$2" == "-" ]; then
    pass=$(cat /etc/passwd | md5sum  | awk {'print $1'})
else
    pass=$2
fi

if [ "$3" == "" ] || [ "$3" == "-" ]; then
    port=27017
else
    port=$3
fi

docker run -td --restart unless-stopped \
    -e MONGO_INITDB_ROOT_USERNAME=ezze \
    -e MONGO_INITDB_ROOT_PASSWORD=$pass \
    -e MONGO_INITDB_DATABASE=ezze \
    --name $name --hostname mongo -p $port:27017 mongo:latest

ip=$(wget -qO- http://icanhazip.com)

echo "mongo service: "$ip":"$port
echo "credentials: ezze / "$pass

