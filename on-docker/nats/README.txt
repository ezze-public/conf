
#
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/nats/README.txt | bash

docker build -t nats-image https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/nats/conf/dockerfile
docker run -t -d --restart unless-stopped --name nats -p 4222:4222 -p 6222:6222 -p 8222:8222 --hostname nats nats-image
