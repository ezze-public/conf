
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/vsftpd-on-docker/README.txt | bash

docker rm -f vsftpd
docker rmi vsftpd-image

docker build -t vsftpd-image https://gitlab.com/ezze-public/conf/-/raw/main/vsftpd-on-docker/conf/dockerfile
docker run -t -d --restart unless-stopped --name vsftpd --hostname vsftpd -v /home:/home -p 60000-60009:60000-60009 -p 20:20 -p 21:21 -p 22:22 vsftpd-image

