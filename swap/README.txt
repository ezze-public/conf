
#
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/swap/README.txt | bash -s 4G

mem=$1
if [ "$mem" == "" ]; then
    mem="2G"
fi

fallocate -l $mem /swapfile
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
cp /etc/fstab /etc/fstab.bk
echo '' >> /etc/fstab
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab
free -m

