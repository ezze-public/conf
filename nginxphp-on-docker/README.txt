
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/nginxphp-on-docker/README.txt | bash

docker build -t nginxphp https://gitlab.com/ezze-public/conf/-/raw/main/nginxphp-on-docker/conf/dockerfile
docker run -t -d --restart unless-stopped --name the-host -p 30022:22 -p 30080:80 -p 30433:443 nginxphp
