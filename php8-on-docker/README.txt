#
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/php8-on-docker/README.txt | bash -s nginx

docker build -t php8 https://gitlab.com/ezze-public/conf/-/raw/main/php8-on-docker/conf/dockerfile
docker run -t -d --restart unless-stopped -v /docker/$1/home:/home -p 22908:22 --net rick --hostname php8 --name php8 php8

