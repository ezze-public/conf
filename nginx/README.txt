
#
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/nginx/README.txt | bash -s 80 </var/www/html>

port=$1
home=$2
wild=$3

apt -y install nginx

rm -rf /etc/nginx/sites-enabled
ln -s /etc/nginx/sites-available /etc/nginx/sites-enabled

wget https://gitlab.com/ezze-public/conf/-/raw/main/nginx/conf/nginx.conf -O /etc/nginx/nginx.conf


# wild
if [ "$wild" == "wildcard" ]; then
    wget https://gitlab.com/ezze-public/conf/-/raw/main/nginx/conf/default-wildcard -O /etc/nginx/sites-enabled/default
else
    wget https://gitlab.com/ezze-public/conf/-/raw/main/nginx/conf/default -O /etc/nginx/sites-enabled/default
fi

rm -rf /var/www/html/index.nginx-debian.html

# port
sed -i 's/listen 80;/listen '$port';/g' /etc/nginx/sites-enabled/default

# home
if [ "$home" != "" ] && [ "$home" != "-" ] && [ "$home" != "/var/www/html" ]; then
    sed -i 's,/var/www/html,'$home',g' /etc/nginx/sites-enabled/default
    rm -rf /var/www/html
    mkdir -p $home
fi

service nginx reload

echo "/usr/sbin/nginx -g 'daemon off;' &" >> /etc/rc.local
