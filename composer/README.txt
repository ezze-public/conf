
#
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/composer/README.txt | bash

if [ `which php | wc -l` == 0 ]; then
  echo "PHP not installed"
  exit
fi

if [ ! -f /usr/bin/composer ]; then

    cd /root

    apt -y install wget
    wget https://getcomposer.org/installer -O composer-setup.php
    # php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    
    php composer-setup.php
    php -r "unlink('composer-setup.php');"

    if [ ! -d "/usr/src/composer" ]; then
        mkdir /usr/src/composer
    fi

    mv composer.phar /usr/src/composer/
    ln -s /usr/src/composer/composer.phar /usr/bin/composer

fi

