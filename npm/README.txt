
# 
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/npm/README.txt | bash -s 20

apt -y purge nodejs &&\
rm -r /etc/apt/sources.list.d/nodesource.list &&\
rm -r /etc/apt/keyrings/nodesource.gpg

apt -y update
apt -y install ca-certificates curl gnupg
mkdir -p /etc/apt/keyrings
curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg

NODE_MAJOR=$1
echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list

apt -y update
apt -y install nodejs

echo ""
echo "###" 
echo "nodejs: "
node -v
echo "npm: "
npm -v
echo "####"
echo ""

