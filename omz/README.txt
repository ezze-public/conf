
#
# wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/omz/README.txt | bash

apt -y update
apt -y install zsh wget

echo y | sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"

# source ~/.zshrc
chsh -s $(which zsh)
zsh
